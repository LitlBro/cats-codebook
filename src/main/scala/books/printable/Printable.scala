package books.printable

import cats.Show

object Printable {
  def format[A](value: A)(implicit showable: Show[A]): String = showable.show(value)
  def print[A](value: A)(implicit showable: Show[A]): Unit = println(format(value))
}