package books.printable
import cats._
import cats.implicits._

object PrintableTest {

  def main(args: Array[String]): Unit = {
    val catOne = Cat("garfield", 11, "orange")
    catOne.show
  }

  final case class Cat(name: String, age: Int, color: String)

  case object Cat {
    implicit val catPrintable: Show[Cat] = Show.show { cat =>
      val name = cat.name.show
      val age = cat.age.show
      val color = cat.color.show
      s"$name is a $age year-old $color cat."
    }
  }
}
